<?php

namespace app\templates\frontend\base\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BaseAsset extends AssetBundle
{
    public $sourcePath = '@app/templates/frontend/base/web/';
    public $basePath = '@app/templates/frontend/base/web/';

    public $css = [
        'css/style.css',
        'css/animate.css',
    ];

    public $js = [
        'js/script.js',
        'js/form.js',
        'js/readmore.js',
        'js/jquery.scroolly.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesome',
        'app\templates\frontend\base\assets\WowAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
    //    disableCache
    public $publishOptions = [
        'forceCopy'=>true,
    ];
}