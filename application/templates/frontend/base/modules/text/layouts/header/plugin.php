<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'header',
    'title' => Yii::t('text', 'Header'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/header/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/header/view.php',
    'settings' => [
        'logo' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/logo-header.png'
        ],
        'menuHome' => [
            'type' => 'menuType',
            'value' => '5'
        ],
        'currencyTitle' => [
            'type' => 'textInput',
            'value' => 'Курс валют на'
        ],
        'phone' => [
            'type' => 'textInput',
            'value' => '+38 099 405 55 00'
        ],
        'mail' => [
            'type' => 'textInput',
            'value' => 'tavaagroinvest@gmail.com'
        ]
    ],
];
