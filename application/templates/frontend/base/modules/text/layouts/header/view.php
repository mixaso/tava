<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
$prod = new \app\modules\shop\models\Product();
$data = $prod->getValute();
$date = new DateTime();
$datenow = $date->format('d.m.Y');
?>
<header class="header">
    <div class="header__cntnr">
        <a class="navbar-brand animated zoomIn" href="/"><img src="<?= $model->settings['logo']['value'] ?>" alt=""></a>
        <nav class="navbar navbar-default header__navbar">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse header__navbar_collapse" id="bs-example-navbar-collapse-1">
                <?php
                $items = ApiMenu::getMenuLvl($model->getSetting('menuHome'), 1, 2);
                echo \yii\bootstrap\Nav::widget([
                    'items' => $items,
                    'options' => [
                        'class' => 'nav navbar-nav',
                        'id' => 'navlist'
                    ]
                ]);
                //            echo Menu::widget();
                ?>
            </div><!-- /.navbar-collapse -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        </nav>
        <div class="header__right">
            <div class="navbar__right">
                <div class="navbar__right__top">
                    <div class="navbar__right__top_currency">
                        <div>
                    <span>
                        <?= $model->settings['currencyTitle']['value'].' '.$datenow ?>
                    </span>
                        </div>
                        <div>
                    <span class="dol">
                        USD: <span><?=round($data['USD'],2)?></span>
                    </span>
                    <span class="euro">
                        EUR: <span><?=round($data['EUR'],2)?></span>
                    </span>
                        </div>
                    </div>
                    <div class="navbar__right__top_lang">
                        <a class="<?= (Yii::$app->language == 'ru-RU') ? 'active':'' ?>"
                           href="<?= Url::toRoute(['/', '__language' => 'ru']) ?>">РУС</a>&nbsp;/&nbsp;<a class="<?= (Yii::$app->language == 'uk-UA') ? 'active':'' ?>"
                                                                                                          href="<?= Url::toRoute(['/', '__language' => 'ua']) ?>">УКР</a>
                    </div>
                </div>
                <div class="navbar__right__bottom">
                    <div class="navbar__right__bottom_phone">
                        <a href="tel:<?= $model->settings['phone']['value'] ?>">
                            <?= $model->settings['phone']['value'] ?>
                        </a>
                    </div>
                    <div class="navbar__right__bottom_mail">
                        <a href="mailto:<?= $model->settings['mail']['value'] ?>">
                            <?= $model->settings['mail']['value'] ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>