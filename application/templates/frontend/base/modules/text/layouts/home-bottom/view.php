<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

/**
 * @var $model \app\modules\text\models\Text
 */

use yii\helpers\Html;
?>
<div id="<?= $model->getSetting('id') ?>">
    <div class="container <?= $model->getSetting('cssClass') ?>">
        <?php if((int)$model->getSetting('hideTitle') !== 1): ?>
            <?= Html::tag($model->getSetting('headingSize'), $model->title, ['class' => 'home_ttl']) ?>
            <?php if ($model->subtitle != ''): ?>
                <h4 class="home_dsc"><?= $model->subtitle ?></h4>
            <?php endif; ?>
        <?php endif; ?>
        <div class="home_cntnt">
            <?= $model->text ?>
        </div>
    </div>
</div>
