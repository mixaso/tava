<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use yii\helpers\Html;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="modal fade" id="<?= $model->getSetting('id') ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $model->getSetting('id') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body">
                [form_builder id="<?= $model->getSetting('form') ?>"]
            </div>
        </div>
    </div>
</div>
