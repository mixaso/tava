<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<section class="actions" style="background: url('<?= Url::toRoute($model->settings['background']['value'], true) ?>') center; background-size: cover;">
    <div class="container">
        <h2 class="actions_heading"><?= $model->settings['heading']['value'] ?></h2>
        <div class="actions__content">
            <div class="col-sm-4">
                <div class="actions__content_blc">
                    <div class="actions__content_blc_img" style="background: url('<?= $model->settings['firstBlockImage']['value'] ?>') center; background-size: cover;">
                </div>
                    <div class="actions__content_blc_ttl">
                        <a href="<?= $model->settings['firstBlockButtonLink']['value'] ?>">
                            <?= $model->settings['firstBlockTitle']['value'] ?>
                        </a>
                    </div>
                    <div class="actions__content_blc_text">
                    <span>
                        <?= $model->settings['firstBlockContent']['value'] ?>
                    </span>
                    </div>
                    <a href="<?= $model->settings['firstBlockButtonLink']['value'] ?>" class="btn btn-default actions__content_blc_btn"><?= $model->settings['firstBlockButtonText']['value'] ?></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="actions__content_blc">
                    <div class="actions__content_blc_img"  style="background: url('<?= $model->settings['secondBlockImage']['value'] ?>') center; background-size: cover;">
                    </div>
                    <div class="actions__content_blc_ttl">
                        <a href="<?= $model->settings['secondBlockButtonLink']['value'] ?>">
                            <?= $model->settings['secondBlockTitle']['value'] ?>
                        </a>
                    </div>
                    <div class="actions__content_blc_text">
                    <span>
                        <?= $model->settings['secondBlockContent']['value'] ?>
                    </span>
                    </div>
                    <a href="<?= $model->settings['secondBlockButtonLink']['value'] ?>" class="btn btn-default actions__content_blc_btn"><?= $model->settings['secondBlockButtonText']['value'] ?></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="actions__content_blc">
                    <div class="actions__content_blc_img" style="background: url('<?= $model->settings['thirdBlockImage']['value'] ?>') center; background-size: cover;">
                    </div>
                    <div class="actions__content_blc_ttl">
                        <a href="<?= $model->settings['thirdBlockButtonLink']['value'] ?>">
                            <?= $model->settings['thirdBlockTitle']['value'] ?>
                        </a>
                    </div>
                    <div class="actions__content_blc_text">
                    <span>
                        <?= $model->settings['thirdBlockContent']['value'] ?>
                    </span>
                    </div>
                    <a href="<?= $model->settings['thirdBlockButtonLink']['value'] ?>" class="btn btn-default actions__content_blc_btn"><?= $model->settings['thirdBlockButtonText']['value'] ?></a>
                </div>
            </div>
        </div>
    </div>
</section>