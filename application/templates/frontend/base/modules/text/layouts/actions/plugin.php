<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'actions',
    'title' => Yii::t('text', 'Actions'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/actions/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/actions/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Наші акційні пропозиції'
        ],
        'background' => [
            'type' => 'mediaInput',
            'value' => '/uploads/FrontEnd/actions-bg.jpg'
        ],
        'firstBlockImage' => [
            'type' => 'mediaInput',
            'value' => '/upload/FrontEnd/action1.jpg'
        ],
        'firstBlockTitle' => [
            'type' => 'textInput',
            'value' => 'Акційна пропозиція'
        ],
        'firstBlockContent' => [
            'type' => 'textInput',
            'value' => 'Пропонуємо посівний матеріал від виробника по най нижчих цінам'
        ],
        'firstBlockButtonLink' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'firstBlockButtonText' => [
            'type' => 'textInput',
            'value' => 'Докладніше'
        ],
        'secondBlockImage' => [
            'type' => 'mediaInput',
            'value' => '/upload/FrontEnd/action2.jpg'
        ],
        'secondBlockTitle' => [
            'type' => 'textInput',
            'value' => 'Акційна пропозиція'
        ],
        'secondBlockContent' => [
            'type' => 'textInput',
            'value' => 'Пропонуємо посівний матеріал від виробника по най нижчих цінам'
        ],
        'secondBlockButtonLink' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'secondBlockButtonText' => [
            'type' => 'textInput',
            'value' => 'Докладніше'
        ],
        'thirdBlockImage' => [
            'type' => 'mediaInput',
            'value' => '/upload/FrontEnd/action3.jpg'
        ],
        'thirdBlockTitle' => [
            'type' => 'textInput',
            'value' => 'Акційна пропозиція'
        ],
        'thirdBlockContent' => [
            'type' => 'textInput',
            'value' => 'Пропонуємо посівний матеріал від виробника по най нижчих цінам'
        ],
        'thirdBlockButtonLink' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'thirdBlockButtonText' => [
            'type' => 'textInput',
            'value' => 'Докладніше'
        ]
    ],
];
