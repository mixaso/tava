<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
//echo '<pre>';print_r($model);echo '</pre>';
?>

<section class="contacts container-fluid" style="background: url('<?= $model->getSetting('background') ?>') center; background-size: cover;">
    <div class="container contacts_cntnr">
        <h2 class="contacts_ttl">
            <?= $model->getSetting('title') ?>
        </h2>
        <div class="contacts_descr">
            <span>
                <?= $model->getSetting('description') ?>
            </span>
        </div>
        <div class="contacts__task text-right">
            <div class="contacts__task_txt">
                <span>
                    <?= $model->getSetting('taskText') ?>
                </span>
            </div>
            <button class="btn btn-default contacts__task_btn ripplelink" data-toggle="modal" data-target="#<?= $model->getSetting('formLink') ?>"><?= $model->getSetting('formText') ?></button>
        </div>
    </div>
</section>