<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'otherTopBlock',
    'title' => Yii::t('text', 'Other Top Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/otherTopBlock/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/otherTopBlock/view.php',
    'settings' => [
        'background' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/contacts-bg.jpg'
        ],
        'title' => [
            'type' => 'textInput',
            'value' => 'КОНТАКТИ'
        ],
        'description' => [
            'type' => 'textInput',
            'value' => 'Ми пропонуємо посівний матеріал з асортиментом 5000 найменувань виробництва України, Австрії, Німеччини'
        ],
        'logo' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/logo-header.png'
        ],
        'taskText' => [
            'type' => 'textInput',
            'value' => 'Потрібна консультація фахівця?'
        ],
        'formLink' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'formText' => [
            'type' => 'textInput',
            'value' => 'Задати питання'
        ]
    ],
];
