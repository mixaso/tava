<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'footer',
    'title' => Yii::t('text', 'Footer'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/footer/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/footer/view.php',
    'settings' => [
        'menu' => [
            'type' => 'menuType',
            'value' => '5'
        ],
        'formLink' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'formText' => [
            'type' => 'textInput',
            'value' => 'Задати питання'
        ],
        'copyright' => [
            'type' => 'textInput',
            'value' => '&copy;2017 ТзОВ &laquo;ТАВАГРОІНВЕСТ&raquo;<br>Продаж зарнових, насіння, добрив гуртом і в роздріб'
        ],
        'address' => [
            'type' => 'textInput',
            'value' => 'м.Полтава, вул Серьогіна, буд.8, оф.1'
        ],
        'phone' => [
            'type' => 'textInput',
            'value' => '+38 099 405 55 00'
        ],
        'mail' => [
            'type' => 'textInput',
            'value' => 'tavaagroinvest@gmail.com'
        ]
    ],
];
