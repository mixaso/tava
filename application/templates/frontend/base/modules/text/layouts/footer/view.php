<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<footer class="footer">
    <section class="footer__nav">
        <div class="container">
            <nav class="navbar navbar-default footer__navbar">
                <button class="btn btn-default footer__navbar_btn ripplelink navbar-right" data-toggle="modal" data-target="#<?= $model->getSetting('formLink') ?>"><?= $model->getSetting('formText') ?></button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse footer__navbar_collapse" id="bs-example-navbar-collapse-1">
                    <?php
                    $items = ApiMenu::getMenuLvl($model->getSetting('menu'), 1, 1);
                    echo \yii\bootstrap\Nav::widget([
                        'items' => $items,
                        'options' => [
                            'class' => 'nav navbar-nav',
                            'id' => 'navlist'
                        ]
                    ]);
                    //            echo Menu::widget();
                    ?>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </section>
    <section class="footer__contacts">
        <div class="container">
            <div class="col-sm-6 footer__contacts_col">
                <div class="footer__contacts_copyright">
                <span>
                    <?= $model->settings['copyright']['value'] ?>
                </span>
                </div>
                <div class="footer__contacts_address">
                <span>
                    <?= $model->settings['address']['value'] ?>
                </span>
                </div>
            </div>
            <div class="col-sm-6 footer__contacts_col">
                <div class="footer__contacts_phone">
                    <a href="tel:<?= $model->settings['phone']['value'] ?>">
                        <?= $model->settings['phone']['value'] ?>
                    </a>
                </div>
                <div class="footer__contacts_mail">
                    <a href="mailto:<?= $model->settings['mail']['value'] ?>">
                        <?= $model->settings['mail']['value'] ?>
                    </a>
                </div>
            </div>
        </div>
    </section>
</footer>
<a href="#" id="back-to-top" title="Back to top"><i class="glyphicon glyphicon-upload"></i></a>