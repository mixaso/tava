<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'news',
    'title' => Yii::t('text', 'News'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/news/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/news/view.php',
    'settings' => [
        'newsCategory' => [
            'type' => 'select',
            'items' => function() {
                $items = \app\modules\content\models\ContentCategory::find()->published()->all();
                $items = \yii\helpers\ArrayHelper::map($items, 'id', 'title');
                return $items;
            },
            'value' => ''
        ]
    ],
];
