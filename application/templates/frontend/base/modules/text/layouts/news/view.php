<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\content\models\ContentArticles;
use app\modules\content\models\ContentArticlesLang;

$news = ContentArticles::find()
    ->joinWith(['translations'])
    ->andWhere([
        ContentArticles::tableName().'.category_id'  => $model->getSetting('newsCategory'),
        ContentArticles::tableName().'.status'       => ContentArticles::STATUS_PUBLISHED,
        ContentArticlesLang::tableName().'.language' => Yii::$app->language
    ])
    ->orderBy(['published_at' => SORT_DESC])
    ->limit(3)
    ->all();
?>
<?php if(count($news) > 0): ?>

<section class="news">
    <?php foreach ($news as $new): ?>
        <div class="container news__cntnr">
            <div class="col-sm-5 news__cntnr_img" style="background: url('<?= $new->getUploadUrl('image') ?>') center; background-size: cover;">
            </div>
            <div class="col-sm-7 news__cntnr__cntnt">
                <div class="news__cntnr__cntnt_date">
                    <?= date('d.m.Y', $new->published_at) ?>
                </div>
                <div class="news__cntnr__cntnt_ttl">
                    <?= $new->title ?>
                </div>
                <div class="news__cntnr__cntnt_cntnt">
                    <?= $new->content ?>
                </div>
                <a class="btn btn-default news__cntnr_btn" href="<?= Url::to($new->frontendViewLink) ?>">Докладніше</a>
            </div>
        </div>
    <?php endforeach; ?>
</section>
<?php endif; ?>