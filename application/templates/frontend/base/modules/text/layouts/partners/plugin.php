<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'partners',
    'title' => Yii::t('text', 'Partners'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/partners/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/partners/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Ми представляємо продукцію'
        ],
        'widget' => [
            'type' => 'widgetkit',
            'value' => ''
        ],
    ],
];
