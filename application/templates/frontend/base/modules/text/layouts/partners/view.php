<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<section class="partners">
    <div class="container">
        <h2 class="partners_heading"><?= $model->settings['heading']['value'] ?></h2>
        <div class="partners__content">
            [widgetkit id="<?= $model->getSetting('widget') ?>"]
        </div>
    </div>
</section>