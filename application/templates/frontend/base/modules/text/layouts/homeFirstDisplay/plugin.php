<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'homeFirstDisplay',
    'title' => Yii::t('text', 'Home First Display'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/homeFirstDisplaypreview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/homeFirstDisplay/view.php',
    'settings' => [
        'firstDisplayCategoriesBackground' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/firstDisplay-bg.jpg'
        ],
        'firstDisplayCategoriesFirstMenu' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'firstDisplayCategoriesFirstMenuImage' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/sowing-materials.png'
        ],
        'firstDisplayCategoriesFirstMenuText' => [
            'type' => 'textInput',
            'value' => 'посівний матеріал'
        ],
        'firstDisplayCategoriesSecondMenu' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'firstDisplayCategoriesSecondMenuImage' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/mineral-fertilizers.png'
        ],
        'firstDisplayCategoriesSecondMenuText' => [
            'type' => 'textInput',
            'value' => 'мікродобрива'
        ],
        'firstDisplayCategoriesThirdMenu' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'firstDisplayCategoriesThirdMenuImage' => [
            'type' => 'mediaInput',
            'value' => 'uploads/FrontEnd/protection-of-plants-protection.png'
        ],
        'firstDisplayCategoriesThirdMenuText' => [
            'type' => 'textInput',
            'value' => 'засоби захисту рослин'
        ],
        'firstDisplayCategoriesAll' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'firstDisplayCategoriesAllText' => [
            'type' => 'textInput',
            'value' => 'перейти до каталогу'
        ]
    ],
];
