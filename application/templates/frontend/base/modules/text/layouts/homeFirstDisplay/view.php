<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;

?>
<section class="firstDisplay">

    <div class="firstDisplay__categories" style="background: url('<?= $model->settings['firstDisplayCategoriesBackground']['value'] ?>') center; background-size: cover;">
        <div class="firstDisplay__categories_section">
            <a href="<?= $model->settings['firstDisplayCategoriesFirstMenu']['value'] ?>" id="cat_1" class="firstDisplay__categories_cntnr firstDisplay__categories_cat_1 animated zoomIn">
                <div class="firstDisplay__categories_cntnr_round">
                    <div class="firstDisplay__categories_cntnr_round_in">
                        <img src="<?= $model->settings['firstDisplayCategoriesFirstMenuImage']['value'] ?>" alt="">
                    </div>
                </div>
                <span>
                    <?= $model->settings['firstDisplayCategoriesFirstMenuText']['value'] ?>
                </span>
            </a>
            <a href="<?= $model->settings['firstDisplayCategoriesSecondMenu']['value'] ?>" class="firstDisplay__categories_cntnr firstDisplay__categories_cat_2 animated zoomIn">
                <div class="firstDisplay__categories_cntnr_round">
                    <div class="firstDisplay__categories_cntnr_round_in">
                        <img src="<?= $model->settings['firstDisplayCategoriesSecondMenuImage']['value'] ?>" alt="">
                    </div>
                </div>
                <span>
                    <?= $model->settings['firstDisplayCategoriesSecondMenuText']['value'] ?>
                </span>
            </a>
            <a href="<?= $model->settings['firstDisplayCategoriesThirdMenu']['value'] ?>" class="firstDisplay__categories_cntnr firstDisplay__categories_cat_3 animated zoomIn">
                <div class="firstDisplay__categories_cntnr_round">
                    <div class="firstDisplay__categories_cntnr_round_in">
                        <img src="<?= $model->settings['firstDisplayCategoriesThirdMenuImage']['value'] ?>" alt="">
                    </div>
                </div>
                <span>
                    <?= $model->settings['firstDisplayCategoriesThirdMenuText']['value'] ?>
                </span>
            </a>
            <a href="<?= $model->settings['firstDisplayCategoriesAll']['value'] ?>" class="firstDisplay__categories_all firstDisplay__categories_cat_all animated zoomIn"><?= $model->settings['firstDisplayCategoriesAllText']['value'] ?></a>
        </div>
    </div>
</section>