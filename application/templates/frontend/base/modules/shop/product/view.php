<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

/**
 * @var $this  \app\components\CoreView;
 * @var $model \app\modules\shop\models\Product;
 */

use yii\helpers\Html;
use app\modules\shop\models\Category;
use app\modules\text\api\Text;


$modification = $model->getModification();

$this->setSeoData($model->name);

Yii::$app->opengraph->set([
    'title'       => $model->name,
    'description' => $model->short_text,
    'image'       => '',
]);

if (isset($parent_cat[0]->slug)) {
    $this->params['breadcrumbs'][] = [
        'url' => ['/shop/category/view', 'slug' => $parent_cat[0]->slug],
        'label' => $parent_cat[0]->name,
    ];
}

$this->bodyClass[] = 'product_page';
$this->params['breadcrumbs'][] = [
    'url'   => ['/shop/category/view', 'slug' => $model->category->slug],
    'label' => $model->category->name,
];

$menu = Yii::$app->menuManager->getActiveMenu();
$this->params['breadcrumbs'][] = $model->name;

if(count($model->modifications)) {
    $img = $model->modifications[0]->getImage()->getUrl('');
} else {
    $img = $model->getImage()->getUrl('');
}
$miniImg = $model->category->getField("miniImg");
$img_tag = Html::img($img, [
    'class' => 'product-photo__img',
    'alt' => $model->name,
    'title' => $model->name,
    'data-product-photo' => true,
    'style' => 'max-width: 310px',
]);
?>

<div class="row container product__row">
    <div class="col-md-3 categoryMenu_col">
        <button id="sidebarCollapse" type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div id="categoryMenu" class="categoryMenu">
            <?=\app\modules\shop\widgets\ShowTreeCategory::widget();?>
        </div>
    </div>
    <div class="col-md-9 categoryMenu_list">
        <?php if(isset($this->params['breadcrumbs'])): ?>
            <div class="breadcrumbs">
                <?= \yii\widgets\Breadcrumbs::widget([
                    'options' => ['class' => 'inline-layout'],
                    'itemTemplate' => "<li>{link}</li>\n",
                    'activeItemTemplate' => "<li><span>{link}</span></li>\n",
                    'links' => $this->params['breadcrumbs'],
                ]);?>
            </div>
            <?php $this->params['breadcrumbs'] = null; ?>
        <?php endif; ?>
        <div class="product-cntnr clearfix">
            <div class="product-cntnr__imgs">
                <div class="product-cntnr__imgs_in">
                    <?= Html::a(
                        $img_tag,
                        $img,
                        ['class' => 'mainThumb product-cntnr__imgs_img', 'rel' => 'group', 'style' => ['background' =>"url('$img'), #ffffff", 'background-position' => 'center', 'background-size' => 'contain', 'background-repeat' => 'no-repeat']]
                    ) ?>
                    <?= Html::tag(
                        'div',
                        '',
                        ['class' => 'product-cntnr__imgs_cat-img', 'style' => ['background' => "url('$miniImg'), #ffffff", 'background-position' => 'center', 'background-size' => 'contain', 'background-repeat' => 'no-repeat']]
                    ) ?>
                    <div class="slider-nav product-cntnr__slide thumbnails">
                        <? foreach ($model->getImages() as $imgg): ?>
                            <a href="/uploads/store/<?= $imgg->filePath ?>" class="col-sm-3 col-xs-6"
                               tabindex="0" style="background: url('/uploads/store/<?= $imgg->filePath ?>') no-repeat, #fff; background-size: contain; background-position: center;">
                            </a>
                        <?endforeach; ?>
                    </div>
                    <div style="display:none;" class="slider-nav product-cntnr__slide">
                        <? foreach ($model->getImages() as $imgg): ?>
                            <a href="/uploads/store/<?= $imgg->filePath ?>" rel="group" onclick="return false;" class="mainThumb col-sm-3 col-xs-6"
                               tabindex="0" style="background: url('/uploads/store/<?= $imgg->filePath ?>') no-repeat, #fff; background-size: contain; background-position: center;">
                            </a>
                        <?endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="product-cntnr__descriptions">
                <div class="product-cntnr__title">
                    <span><?= $model->name ?></span>
                </div>
                <div class="product-cntnr__description">
                    <?= $model->short_text ?>
                </div>
                <?php if ($model->producer): ?>
                    <div class="product-cntnr__manufacturer text-left">
                        <?= Yii::t('shop', 'Producer'); ?>: <?= Html::a($model->producer->name, ['/shop/producer/view', 'slug' => $model->producer->slug]) ?>
                    </div>
                <?php endif ?>
                <div class="product-cntnr__text">
                    <?= $model->text ?>
                </div>
                <div class="product-cntnr__cost">
                    <span class="product-price__item-value"><?=$model->getPrice()==0 ? '':$model->getPrice()?></span><span class="product-price__item-value-val">грн./</span>
                    <span class="product-cntnr__item-cur"><?=$model->getProduct()->getField('dimension');?></span>
                </div>
                <?php
                    $buy = Yii::t('shop', 'Buy');
                    $modalFormSale = Yii::t('shop', 'modalFormSale');
                ?>
                <?= Html::a("<span>$buy</span>", '#', [
                    'class' => 'btn btn-default product-cntnr__btns_order',  'data-toggle' => 'modal',             'data-target' => "$modalFormSale"
                ]) ?>
            </div>
        </div>
        <div class="product-cntnr__otherDescriptions">
            <?=$model->getProduct()->getField('otherDescriptions');?>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="application/templates/frontend/base/web/js/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="application/templates/frontend/base/web/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />

<script type="text/javascript">
    $j = jQuery.noConflict();
      $j(document).ready(function() {
        $j(".mainThumb").fancybox({
            'cyclic' : true
        });

          $j('.thumbnails a').click(function(){                                   // При нажатиина миниатюру
              var images = $(this).attr('href');

              $j(".product-photo__img").attr({ src: images });
              $j("a.product-cntnr__imgs_img").css('background-image', 'url("'+images+'")');// Подменяем адрес большого изображения на адрес выбранного               // Добавляем класс .active на выбранную миниатюру
              return false;
          });

    });
</script>


