<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 30.12.2016
 * Project: oakcms
 * File name: _item.php
 *
 * @var $model \app\modules\shop\models\Product;
 */
use yii\helpers\Html;
$modification = $model->getModification();

if(count($model->modifications)) {
    $img = $model->modifications[0]->getImage()->getUrl('');
} else {
    $img = $model->getImage()->getUrl('');
}
$miniImg = $model->category->getField("miniImg");
?>
<div class="productsList__item col-sm-4">
    <div class="productsList__item__cntnr">
        <?= Html::a(
            '',
            ['/shop/product/view', 'slug' => $model->slug],
            ['class' => 'productsList__item__cntnr_img', 'style' => ['background' => "url('$img'), #ffffff", 'background-position' => 'center', 'background-size' => 'contain', 'background-repeat' => 'no-repeat']]
        ) ?>
        <?= Html::tag(
            'div',
            '',
            ['class' => 'productsList__item__cntnr_cat-img', 'style' => ['background' => "url('$miniImg'), #ffffff", 'background-position' => 'center', 'background-size' => 'contain', 'background-repeat' => 'no-repeat']]
        ) ?>

        <div class="productsList__item__cntnr_title">
            <?= Html::a($model->name, ['/shop/product/view', 'slug' => $model->slug]) ?>
        </div>
        <div class="productsList__item__cntnr_text">
            <div class="product-price__old">
                <span class="product-text"><?= $model->short_text ?></span>
            </div>
        </div>
        <div class="productsList__item__cntnr_price">
            <div class="product-price product-price--bg">
                <?php if(isset($modification) && ($price = $modification->getPrice(2)) && $price > 0): ?>
                    <span class="product-price__item-value"><?= $price ?></span>
                    <span><?= $modification->price ?>P</span>
                <?php else: ?>
                    <div class="product-price__item">
                        <div class="product-price__main">
                            <span class="product-price__item-value"><?=$model->getPrice()==0 ? '':$model->getPrice().' грн./'?></span>
                            <span class="product-price__item-cur"><?=$model->getProduct()->getField('dimension');?></span>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="productsList__item__cntnr_btns clearfix">
            <?php $Moreinf = Yii::t('shop', 'Moreinf');
             $buy = Yii::t('shop', 'Buy');
             $modalFormSale = Yii::t('shop', 'modalFormSale');
            ?>
            <?= Html::a("<span>$Moreinf</span>", ['/shop/product/view', 'slug' => $model->slug], [
                'class' => 'btn btn-default productsList__item__cntnr_btns_more'
            ]) ?>
            <?= Html::a("<span>$buy</span>", ['', 'slug' => $model->slug], [
                'class' => 'btn btn-default productsList__item__cntnr_btns_order',  'data-toggle' => 'modal',             'data-target' => "$modalFormSale"
            ]) ?>
        </div>
    </div>
</div>