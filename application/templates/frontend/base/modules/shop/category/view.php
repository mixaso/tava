<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 11.12.2016
 * Project: oakcms
 * File name: view.php
 *
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\shop\models\Category;
 * @var $productDataProvider \yii\data\ActiveDataProvider;
 */

use yii\helpers\Html;
use app\modules\menu\api\Menu as ApiMenu;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\modules\shop\models\category;
?>

<!--<h1 class="title text-center">--><?//= $model->name ?><!--</h1>-->
<?php
$parent_cat = Category::find()->where(['id'=>$model->parent_id])->all();
if (isset($parent_cat[0]->slug)) {
    $this->params['breadcrumbs'][] = [
        'url' => ['/shop/category/view', 'slug' => $parent_cat[0]->slug],
        'label' => $parent_cat[0]->name,
    ];
}
$menu = Yii::$app->menuManager->getActiveMenu();
$this->params['breadcrumbs'][] = $model->name;
?>

<?php if($model->text != ''):?>
    <div class="descr_page">
        <?= $model->text ?>
    </div>
<?php endif;?>
<div id="catProd" class="row">
    <div class="col-md-3 categoryMenu_col">
        <button id="sidebarCollapse" type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div id="categoryMenu" class="categoryMenu">
            <?=\app\modules\shop\widgets\ShowTreeCategory::widget();?>
        </div>
    </div>
    <div class="col-md-9 row categoryMenu_list">

        <div class="breadcrumbs categoryMenu_list_breadcrumbs">
            <?= \yii\widgets\Breadcrumbs::widget([
                'options' => ['class' => 'inline-layout'],
                'itemTemplate' => "<li>{link}</li>\n",
                'activeItemTemplate' => "<li><span>{link}</span></li>\n",
                'links' => $this->params['breadcrumbs'],
            ]);?>
        </div>
        <?php $this->params['breadcrumbs'] = null; ?>

        <div class="categoryMenu_list_form">
            <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'name')->label('')->textInput(['value' => '','name'=>'product-name']) ?>
                <?= Html::submitButton('', ['class' => 'glyphicon glyphicon-search']) ?>
            <?php ActiveForm::end(); ?>
        </div>

        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $productDataProvider,
            'itemView' => '_item',
            'summary' => false,
            'options' => ['class' => 'productsList'],
        ]); ?>
    </div>
</div>
