<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 21.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $this \app\components\CoreView
 * @var $model \app\modules\content\models\ContentPages;
 */


$this->bodyClass = ['page-'.$model->id];

$this->setSeoData($model->title, $model->description, '');

/** @var \app\modules\menu\models\MenuItem $menu */
$menu = Yii::$app->menuManager->getActiveMenu();
if ($menu) {
    $this->params['breadcrumbs'] = $menu->getBreadcrumbs(false);
}
$this->title = $model->title;
?>

<div class="container">
    <div class="contacts_cntnt">
        <?= $model->content ?>
    </div>
</div>
