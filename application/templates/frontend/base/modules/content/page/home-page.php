<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 21.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\content\models\ContentPages;
 */


$this->bodyClass = ['page-'.$model->id];

$this->setSeoData($model->title, $model->description, '');
?>

<section class="<?= $model->slug ?> container">
    <h2 class="home_ttl">
        <?= $model->title ?>
    </h2>

    <h4 class="home_dsc">
        <?= $model->description ?>
    </h4>

    <div class="home_cntnt readmore">
        <?= $model->content ?>
    </div>
</section>
