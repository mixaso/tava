<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1
 */

/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 24.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\content\models\ContentArticles;
 * @var $categoryModel \app\modules\content\models\ContentCategory;
 * @var $breadcrumbs \yii\widgets\Breadcrumbs;
 * @var $this \yii\web\View;
 */

use yii\helpers\Url;
use yii\helpers\StringHelper;

/** @var \app\modules\menu\models\MenuItem $menu */
$menu = Yii::$app->menuManager->getActiveMenu();
if ($menu) {
    //$this->params['breadcrumbs'] = $menu->getBreadcrumbs(false);
    $this->params['breadcrumbs'][] = [
        'url'   => ['/content/category/view', 'slug' => $model->category->slug],
        'label' => $model->category->title,
    ];
    $this->params['breadcrumbs'][] = $model->title;
}

$this->title = $model->title;

$meta_title = ($model->meta_title != '') ? $model->meta_title : $model->title;
$meta_description = StringHelper::truncate((($model->meta_description != '') ? $model->meta_description : strip_tags($model->description)), '140', '');
$meta_keywords = ($model->meta_keywords != '') ? $model->meta_keywords : implode(', ', explode(' ', $model->title));

$this->setSeoData($meta_title, $meta_description, $meta_keywords);

$this->registerJsFile('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$this->registerJsFile('//yastatic.net/share2/share.js');
?>

<section class="article" itemscope itemtype="http://schema.org/Article">
    <meta itemprop="datePublished" content="<?php date('d.m.Y', $model->published_at) ?>" />

    <?php if(isset($this->params['breadcrumbs'])): ?>
        <div class="breadcrumbs">
            <?= \yii\widgets\Breadcrumbs::widget([
                'options' => ['class' => 'inline-layout'],
                'itemTemplate' => "<li>{link}</li>\n",
                'activeItemTemplate' => "<li><span>{link}</span></li>\n",
                'links' => $this->params['breadcrumbs'],
            ]);?>
        </div>
        <?php $this->params['breadcrumbs'] = null; ?>
    <?php endif; ?>

    <div class="article__head clearfix">
        <h1 class="article__ttl" itemprop="headline"><?= $model->title ?></h1>
    </div>
    <div class="article__img pull-left">
        <img src="<?= $model->getUploadUrl('image') ?>" alt="<?= $model->title ?>">
    </div>
    <div class="article__txt clearfix">
        <div class="description">
            <?= $model->description ?>
        </div>
        <div class="content" itemprop="articleBody">
            <?= $model->content ?>
        </div>
    </div>
    <div class="article__footer clearfix">
        <div class="ya-share2 pull-right" data-services="facebook,gplus,twitter,pocket" data-counter=""></div>
        <div class="article__date pull-left">
            <span>
                <?= date("d/m/Y", $model->published_at) ?>
            </span>
        </div>
    </div>
    [block id='19']
</section>
