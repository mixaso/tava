<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 24.09.2016
 * Project: osnovasite
 * File name: _item.php
 * @var $model \app\modules\content\models\ContentArticles;
 */
?>

<div class="col-sm-5 news__cntnr_img" style="background: url('<?= $model->getUploadUrl('image') ?>') center; background-size: cover;">
</div>
<div class="col-sm-7 news__cntnr__cntnt">
    <div class="news__cntnr__cntnt_date">
        <?= date('d.m.Y', $model->published_at) ?>
    </div>
    <div class="news__cntnr__cntnt_ttl">
        <?= $model->title ?>
    </div>
    <div class="news__cntnr__cntnt_cntnt">
        <?= \yii\helpers\StringHelper::truncateWords(strip_tags($model->content), 50) ?>
    </div>
    <a class="btn btn-default news__cntnr_btn" href="">Докладніше</a>
    <?= \app\modules\admin\widgets\Html::a(Yii::t('text', 'Read more...'), ['/content/article/view', 'catslug' => $model->category->slug, 'slug'=>$model->slug], ['class'=>'btn btn-default news__cntnr_btn'])?>
</div>
