/**
 * Created by michael on 09.05.17.
 */

(function($) {
    var oldval = 0;
    $.fn.spinner = function() {
        this.each(function() {
            var el = $(this);

            // add elements
            el.wrap('<span class="input-group"></span>');
            el.before('<span class="input-group-btn sub"><a class="btn btn-default"' +
                ' href="javascript:void(0)">&or;</a></span>');
            el.after('<span class="input-group-btn add"><a class="btn btn-default"' +
                ' href="javascript:void(0)">&and;</a></span>');


            // substract
            el.parent().on('click', '.sub', function () {
                console.log('min');
                if (el.val() == 1){
                    return;
                }
                else {
                    el.val(--oldval);
                }
            });

            // increment
            el.parent().on('click', '.add', function () {
                console.log('max');
                /* if (el.val() < parseInt(el.attr('max'))) {
                 el.val( function(i, oldval) { return ++oldval; });
                 }*/
                el.val(++oldval);

            });
        });
    };

    $('header a[href^="#"], .top-slider__button[href^="#"], .category a[href^="#"]').on('click', function () {
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top - 100
        }, 1000);
        return false;
    });

    $(document).ready(function () {
        $('input[type="number"]').spinner();
    });
})(jQuery);
