$(document).ready(function() {

    $('.home_cntnt').readmore({
        speed: 1000,
        lessLink: '<a class="readmore_close" href="#">Close</a>',
        moreLink: '<a class="readmore_open" href="#">Read more</a>',
        embedCSS: false
    });

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    var windowsWidth = $(window).width();
    if (windowsWidth > 991) {
        $('#catProd').each(function(){
            var widthestBox = 0;
            $('.categoryMenu_col', this).each(function(){
                if($(this).width() > widthestBox) {
                    widthestBox = $(this).width();
                }
            });
            $('#categoryMenu',this).width(widthestBox);
        });

        $('#catProd').each(function () {
            var highestBox = 0,
                highestBox2 = 0,
                catMenuHi = $('#categoryMenu').height();
            $('.categoryMenu_list', this).each(function () {
                if ($(this).height() > catMenuHi) {
                    highestBox = $(this).height();
                } else {
                    highestBox2 = (catMenuHi - 100);
                }
                ;
            });
            $('.categoryMenu_list', this).height(highestBox2);
            $('.categoryMenu_col', this).height(highestBox);

            if (highestBox > 0) {
                $('#categoryMenu').scroolly([
                    {
                        from: 'el-bottom = vp-bottom',
                        direction: 1,
                        addClass: 'fix_down_scroll',
                        removeClass: 'fix_down fix_up fix_up_scroll',
                    },
                    {
                        to: 'el-bottom = con-bottom',
                        direction: 1,
                        addClass: 'fix_down',
                        removeClass: 'fix_down_scroll fix_up fix_up_scroll',
                    },
                    {
                        from: 'vp-top = el-top',
                        to: 'el-top = con-top -100',
                        direction: -1,
                        addClass: 'fix_up_scroll',
                        removeClass: 'fix_down fix_up fix_down_scroll',
                    },
                    {
                        from: 'el-top = con-top -100',
                        direction: 0,
                        addClass: 'fix_up',
                        removeClass: 'fix_down fix_up_scroll fix_down_scroll',
                    }
                ], $('.categoryMenu_col'));
            }
            ;
        });
    };

    var windowsWidth = $(window).width(),
        listGroupHTML = $('#categoryMenu').html(),
        slideSide = $('#slideSide').html();
    if (windowsWidth < 992) {
        $('#categoryMenu').html('');
        $('#slideSide').html(listGroupHTML + '<br>' + slideSide);
    };

    // when opening the sidebar
    $('#sidebarCollapse').on('click', function () {
        // open sidebar
        $('#slideSide').addClass('active');
        // fade in the overlay
        $('#overlay').fadeIn();
        $('.category_view').css({'overflow':'hidden'});
    });


    // if dismiss or overlay was clicked
    $('#dismiss, .overlay').on('click', function () {
        // hide the sidebar
        $('#slideSide').removeClass('active');
        // fade out the overlay
        $('#overlay').fadeOut();
        $('.category_view').css({'overflow':'auto'});
    });

    $('.navbar__right__top_lang').css('display','none');
});