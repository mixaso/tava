<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

return [
    'Binding to the menu'                => 'Привязка до меню',
    'Binding to the php'                 => 'Прив\'язка по коду PHP',
    'Content'                            => 'Вміст',
    'Create custom block'                => 'Створити кастомнй блок',
    'Custom Block'                       => 'Кастомний блок',
    'Custom Blocks'                      => 'Кастомні блоки',
    'Default'                            => '',
    'Enable php code'                    => 'Включити PHP код',
    'Layout'                             => 'Макет',
    'Links'                              => 'Ссилки',
    'Menu'                               => 'Меню',
    'Menu where will be displayed'       => 'Меню де буде виведено',
    'Menus'                              => 'Меню',
    'Not on the same page'               => 'Ни на одній сторінці',
    'On all pages'                       => 'На всіх сторінках',
    'On all pages, except for the above' => 'На всіх сторінках, крім вибраних',
    'On these pages only'                => 'Тільки на вказаних сторінках',
    'Position'                           => 'Позиція',
    'Read more...'                       => 'Детально',
    'Select Layout'                      => 'Виберіть макет',
    'Select Position'                    => 'Виберіть позицію',
    'Subtitle'                           => 'Підзаголовок',
    'Title'                              => 'Заголовок',
    'Where To Place'                     => 'Де розмістити',
    'Widgetkit'                          => '',
    'php code'                           => 'php код',
];
