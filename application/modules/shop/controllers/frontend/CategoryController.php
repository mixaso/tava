<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

namespace app\modules\shop\controllers\frontend;

use app\modules\shop\models\Category;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use Yii;
use app\modules\shop\models\product\ProductSearch;

class CategoryController extends \app\components\Controller
{
    public function actionIndex() {

    }

    public function actionView($slug) {

        $model = self::findModelBySlug($slug);
        $search = Yii::$app->request->post('product-name');

        if (isset($search)) {
            $q = Html::encode($search);
            $query = $model->searchProducts($q);
        } else {
            $query = $model->getProducts();
        }

        if(\Yii::$app->request->get('filter')) {
            $query = $query->filtered();
        }

        $productDataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => new \yii\data\Sort([
                'attributes' => [
                    'price',
                    'is_promo',
                    'is_popular',
                    'is_new',
                ],
            ]),
            'pagination' => [
                'pageSize' => 24,
            ],
        ]);

        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'productDataProvider' => $productDataProvider,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }

    protected function findModelBySlug($slug)
    {
        if (($model = Category::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }

}
