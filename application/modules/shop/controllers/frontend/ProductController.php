<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

namespace app\modules\shop\controllers\frontend;

use app\modules\shop\models\Product;
use yii\web\NotFoundHttpException;
use app\modules\shop\models\Category;

class ProductController extends \app\components\Controller
{
    public function actionView($slug) {
        $model = self::findModelBySlug($slug);
        /*$parent_cat = Category::find()
            ->leftJoin('oak_shop_product_to_category','oak_shop_category.id=category_id')
            ->where(['product_id'=>$model->id])
            ->andFilterWhere(['>', 'parent_id', 0])
            ->all();*/
          $cat_id = Category::find()->where(['id'=>$model->category_id])->one();
          $parent_cat = Category::find()->where(['id'=>$cat_id->parent_id])->all();

         return $this->render('view', [
            'model' => $model,
            'parent_cat' => $parent_cat
         ]);
    }

    protected function findModel($id)
    {

        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }

    protected function findModelBySlug($slug)
    {
        if (($model = Product::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }

}
