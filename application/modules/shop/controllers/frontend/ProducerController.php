<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 25.05.2017
 * Time: 13:00
 */

namespace app\modules\shop\controllers\frontend;


use app\components\Controller;
use app\modules\shop\models\Producer;
use yii\data\ActiveDataProvider;

class ProducerController extends  Controller
{
    public function actionIndex() {

    }

    public function actionView($slug) {
        $model = self::findModelBySlug($slug);

        $query = $model->getProducts();

        if(\Yii::$app->request->get('filter')) {
            $query = $query->filtered();
        }

        $productDataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => new \yii\data\Sort([
                'attributes' => [
                    'price',
                    'is_promo',
                    'is_popular',
                    'is_new',
                ],
            ]),
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'productDataProvider' => $productDataProvider,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Producer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }

    protected function findModelBySlug($slug)
    {
        if (($model = Producer::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }
}