<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 19.05.2017
 * Time: 8:28
 */

namespace app\modules\shop\widgets;

use app\modules\shop\models\Category;
use app\modules\shop\models\Product;
use yii\helpers\Html;
use app\modules\language\models\Language;
use Yii;

class ShowTreeCategory extends \yii\base\Widget {

    public function run()
    {
        $lang = \Yii::$app->language;
        $categories = Category::find()->where(['language' => $lang])->all();
        $tree =  $this->getMenu($categories);
        return $tree;
    }

    function buildTree($arr,$parent_id = 0,&$str = '')
    {
        $active = Yii::$app->request->get('slug');
        if(empty($arr[$parent_id]))
            return;
        if ($parent_id == 0)
            $str.= '<ul class="list-group">';
        else
            $str.= '<ul class="list-group">';

        for($i = 0; $i < count($arr[$parent_id]);$i++) {
            if ($arr[$parent_id][$i]->slug === $active) {
                $str .= '<li class="list-group-item active">' .
                    Html::a($arr[$parent_id][$i]->name, ['/shop/category/view', 'slug' => $arr[$parent_id][$i]->slug], [
                        'class' => 'product-cut__title-link active'
                    ]);
            } else {
                $str .= '<li class="list-group-item">' .
                    Html::a($arr[$parent_id][$i]->name, ['/shop/category/view', 'slug' => $arr[$parent_id][$i]->slug], [
                        'class' => 'product-cut__title-link'
                    ]);
            }
            $count = Category::find()
                ->leftJoin('oak_shop_product_to_category','oak_shop_category.id=category_id')
                ->where(['category_id'=>$arr[$parent_id][$i]->id])
                ->count();

            //$count = Product::find()->where(['category_id'=>$arr[$parent_id][$i]->id])->count();
            $str.= '<span class="product-cut__title-link_count">('.$count.')</span>';
            $this->buildTree($arr,$arr[$parent_id][$i]->id,$str);
            $str.= '</li>';
        }
        $str.= '</ul>';
    }

    function prepareTree($categories)
    {
        $arr = array();
        foreach($categories as $category)
        {
            if (!$category->parent_id)
                $category->parent_id = 0;
            if(empty($arr[$category->parent_id]))
                $arr[$category->parent_id] = array();
            $arr[$category->parent_id][] = $category;
        }
        return $arr;
    }

    function getMenu($categories)
    {
        $string = '';
        $cat = $this->prepareTree($categories);
        $this->buildTree($cat,0,$string);
        return $string;
    }
} 