<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 21.06.2017
 * Time: 10:43
 */
use yii\helpers\Html;

$this->title = 'Клонировать товар';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\app\modules\shop\assets\BackendAsset::register($this);
?>
<div class="product-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'lang' => $lang
    ]) ?>
</div>