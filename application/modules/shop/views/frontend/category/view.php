<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 11.12.2016
 * Project: oakcms
 * File name: view.php
 *
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\shop\models\Category;
 * @var $productDataProvider \yii\data\ActiveDataProvider;
 */

use yii\helpers\Html;

$this->registerJs($slider, \yii\web\View::POS_END, 'slider');
?>

<h1 class="title text-center"><?= $model->name ?></h1>
<?php if($model->text != ''):?>
<div class="descr_page">
    <?= $model->text ?>
</div>
<?php endif;?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $productDataProvider,
                'itemView' => '_item',
                'summary' => false,
                'options' => ['id' => 'productsList']
            ]); ?>
        </div>
        <div class="col-md-3 col-md-pull-9">
            <?php //echo \app\modules\filter\widgets\FilterPanel::widget(['itemId' => $model->id]);?>

            <?= \app\modules\filter\widgets\FilterPanel::widget([
                'itemId' => $model->id,
                'findModel' => $productDataProvider->query,
                'ajaxLoad' => true,
                'resultHtmlSelector' => '#productsList'
            ]); ?>
        </div>
    </div>
</div>
