<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 11.12.2016
 * Project: oakcms
 * File name: view.php
 *
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\shop\models\Category;
 * @var $productDataProvider \yii\data\ActiveDataProvider;
 */

use yii\helpers\Html;
use app\modules\menu\api\Menu as ApiMenu;

?>

<h1 class="title text-center"><?= $model->name ?></h1>
<?php if($model->text != ''):?>
    <div class="descr_page">
        <?= $model->text ?>
    </div>
<?php endif;?>
<div class="row">
    <div class="col-sm-12 row">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $productDataProvider,
            'itemView' => '_item',
            'summary' => false,
            'options' => ['class' => 'productsList']
        ]); ?>
    </div>
</div>
